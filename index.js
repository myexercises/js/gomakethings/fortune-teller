document.addEventListener('DOMContentLoaded', main);

function main() {

    function Model() {
        const model = [
            'Yes!',
            'No.',
            'Maybe',
            'You\'re unstoppable',
            'Who knows',
            'Ask later',
        ];
        const getRandom = () => Math.floor(Math.random() * model.length);

        this.display = () => model[getRandom()];
    }

    const model = new Model();

    // Helpers
    const createElement = (tag, className) => {
        const element = document.createElement(tag);
        if (className === '') return;
        element.classList.add(className);
        return element;
    }

    const sanitize = (content = '') => {
        const placeholder = createElement('div');
        placeholder.textContent = content;
        return placeholder.innerHTML;
    }

    const insertSanitizedTo = (element, content = '') => element.innerHTML = sanitize(content);

    const appendTo = parent =>
        element =>
            parent.appendChild(element);

    const clearInput = input => input.value = '';
    
    const clearContent = element => insertSanitizedTo(element);

    // Cache DOM elements
    const app = document.getElementById('app');
    const question = document.getElementById('question');
    const answer = document.getElementById('answer');
    answer.setAttribute('aria-live', 'polite')

    // Add event listeners
    app.addEventListener('submit', handleFormSubmit, false);
    question.addEventListener('focus', clearAnswer, false);

    // Actions
    function handleFormSubmit(event) {
        event.preventDefault();
        if (question.value === '') {
            return;
        }
        displayAnswer();
        clearInput(question);
    }

    function displayAnswer() {
        const appendToAnswer = appendTo(answer);

        const strong = createElement('strong');
        insertSanitizedTo(strong, question.value);
        
        const questionPlaceholder = createElement('p', 'answer__question-placeholder');
        appendTo(questionPlaceholder)(strong);
        appendToAnswer(questionPlaceholder);
        
        const answerPlaceholder = createElement('p', 'answer__answer-placeholder');
        insertSanitizedTo(answerPlaceholder, model.display());
        appendToAnswer(answerPlaceholder);
    }

    function clearAnswer() {
        clearContent(answer);
    }
};